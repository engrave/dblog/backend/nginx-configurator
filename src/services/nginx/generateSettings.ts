import * as path from 'path';
import * as fs  from 'fs';
import validateDomainCertificates from '../ssl/validateDomainCertificates';

import getNonSSLTemplate from './templates/non-ssl-template';
import getSSLTemplate from './templates/ssl-template';
import { logger } from "../../submodules/shared-library/utils";

const nginxConfDirectory = "/etc/nginx/conf.d/blogs/";

export default async function generateNginxSettings(domain: string) {

    logger.info(`Generating configuration for ${domain}`);

    const configFilename = `${domain.toLowerCase()}.conf`;
    const configFilePath = path.join(nginxConfDirectory, configFilename);
    const configContent = await generateConfigFileContent(domain);
    
    fs.writeFileSync(configFilePath, configContent);
}

const generateConfigFileContent = async (domain: string) => await validateDomainCertificates(domain) ? getSSLTemplate(domain) : getNonSSLTemplate(domain);
