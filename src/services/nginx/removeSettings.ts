import * as path from 'path';
import * as fs  from 'fs';
import { logger } from "../../submodules/shared-library/utils";

const nginxConfDirectory = "/etc/nginx/conf.d/blogs/";

export default async function removeSettings(domain: string) {

    logger.info({domain}, `Removing configuration for ${domain}`);

    const configFilename = `${domain.toLowerCase()}.conf`;
    const configFilePath = path.join(nginxConfDirectory, configFilename);

    if (!fs.existsSync(configFilePath)) {
        logger.info({domain}, `Configuration for ${domain} does not exist, skipping...`);
        return;
    }

    fs.unlinkSync(configFilePath);
}
