import { microservices } from '../../../submodules/shared-library';

const getNonSSlTemplate = (domain: string) => `
server {
    listen 80;
    server_name ${domain} www.${domain};

    location / {
        resolver 127.0.0.11 valid=10s;
        set $blogRenderer ${microservices.blogs_renderer};
        proxy_pass http://$blogRenderer;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
      
        error_page 500 502 503 504 /service.html;

        location = /service.html {
                internal;
                root /var/www/maintenance;
        }
    }
}
`;

export default getNonSSlTemplate;