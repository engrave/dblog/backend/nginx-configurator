import { microservices } from "../../../submodules/shared-library";

const getSSLTemplate = (domain: string) => (`
server {
    server_name www.${domain} ${domain};
    return 307 https://${domain}$request_uri;
}

server {
    listen 443 ssl;
    http2 on;
    server_name www.${domain};
    
    ssl_session_cache shared:SSL:1m; # holds approx 4000 sessions
    ssl_session_timeout 1h; # 1 hour during which sessions can be re-used.
    ssl_session_tickets off;
    ssl_protocols TLSv1.1 TLSv1.2;
    ssl_buffer_size 4k;
    ssl_stapling on;
    ssl_stapling_verify on;
    
    ssl_trusted_certificate /etc/nginx/certificates/blogs/live/${domain}/fullchain.pem;

    ssl_certificate /etc/nginx/certificates/blogs/live/${domain}/fullchain.pem;
    ssl_certificate_key /etc/nginx/certificates/blogs/live/${domain}/privkey.pem;

    return 307 https://${domain}$request_uri;
}

server {
    listen 443 ssl;
    http2 on;
    server_name ${domain};

    add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";

    add_header X-Frame-Options sameorigin; 
    add_header X-Content-Type-Options nosniff; 
    add_header X-Xss-Protection "1; mode=block";
    
    ssl_session_cache shared:SSL:1m; # holds approx 4000 sessions
    ssl_session_timeout 1h; # 1 hour during which sessions can be re-used.
    ssl_session_tickets off;
    ssl_protocols TLSv1.1 TLSv1.2;
    ssl_buffer_size 4k;
    ssl_stapling on;
    ssl_stapling_verify on;
    
    ssl_trusted_certificate /etc/nginx/certificates/blogs/live/${domain}/fullchain.pem;

    ssl_certificate /etc/nginx/certificates/blogs/live/${domain}/fullchain.pem;
    ssl_certificate_key /etc/nginx/certificates/blogs/live/${domain}/privkey.pem;

    location / {
        resolver 127.0.0.11 valid=10s;
        set $blogRenderer ${microservices.blogs_renderer};
        proxy_pass http://$blogRenderer;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;

        error_page 500 502 503 504 /service.html;

        location = /service.html {
                internal;
                root /var/www/maintenance;
        }
    }
}
`);

export default getSSLTemplate;
