import axios from 'axios';
import { handleServiceError } from '../../submodules/shared-library';
import { microservices } from '../../submodules/shared-library';

export default async (domain: string) => {

    return handleServiceError( async () => {
        const options = {
            method: 'POST',
            data: {
                domain: domain
            },
            url: `http://${microservices.ssl}/ssl/validate`
        };
    
        const response = await axios(options);
    
        return response.data.ssl_exists;
    }) 

}