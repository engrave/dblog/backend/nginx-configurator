import healthApi from "../routes/health/health.routes";
import configurationApi from "../routes/configuration/configuration.routes";
import { endpointLogger } from "../submodules/shared-library/utils/logger";

function routes(app:any) {
    app.use('/health', healthApi);
    app.use('/configuration', endpointLogger, configurationApi);
}

export default routes;
