import { Request, Response } from 'express';
import { handleResponseError } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import generateNginxSettings from '../../../services/nginx/generateSettings';

const middleware: any[] =  [
    body('domain').isString().isURL()
];

async function handler(req: Request, res: Response) {

    return handleResponseError(async () => {

        const { domain } = req.body;

        await generateNginxSettings(domain);
        
        return res.json({
            message: 'success'
        });

    }, req, res);

}

export default {
    middleware,
    handler
}