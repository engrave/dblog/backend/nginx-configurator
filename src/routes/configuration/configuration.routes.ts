import * as express from "express";
import generate from "./routes/generate";
import remove from "./routes/remove";

const configurationApi: express.Router = express.Router();

configurationApi.post('/generate', generate.middleware, generate.handler);
configurationApi.post('/remove', remove.middleware, remove.handler);

export default configurationApi;
