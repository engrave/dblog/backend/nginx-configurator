import app from './app/app';
import { waitForMicroservice, logger, microservices, listenOnPort, mongo } from './submodules/shared-library';
import { Blogs } from './submodules/shared-library/models/Blogs';
import generateNginxSettings from './services/nginx/generateSettings';
import * as mongoose from 'mongoose';

( async () => {

    try {
        await mongoose.connect(mongo.uri, mongo.options);

        await waitForMicroservice(microservices.ssl);
        
        const blogs = await Blogs.find({custom_domain: { $ne: null }});
        
        logger.info(blogs);

        for(const blog of blogs) {
            await generateNginxSettings(blog.custom_domain);
        }

        listenOnPort(app, 3000);
        
    } catch (error) {
        logger.error('Error encountered while starting the application: ', error.message);
        process.exit(1);
    }

})();

